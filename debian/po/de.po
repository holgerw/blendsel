#
#    Translators, if you are not familiar with the PO format, gettext
#    documentation is worth reading, especially sections dedicated to
#    this format, e.g. by running:
#         info -n '(gettext)PO Files'
#         info -n '(gettext)Header Entry'
#    Some information specific to po-debconf are available at
#            /usr/share/doc/po-debconf/README-trans
#         or http://www.debian.org/intl/l10n/po-debconf/README-trans
#    Developers do not need to manually edit POT or PO files.
#
#    Dennis Stampfer <seppy@debian.org>, 2004.
# Holger Wansing <hwansing@mailbox.org>, 2025.
msgid ""
msgstr ""
"Project-Id-Version: blendsel/debian/po\n"
"Report-Msgid-Bugs-To: blendsel@packages.debian.org\n"
"POT-Creation-Date: 2025-01-09 09:02+0100\n"
"PO-Revision-Date: 2025-01-09 09:07+0100\n"
"Last-Translator: Holger Wansing <hwansing@mailbox.org>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 22.12.3\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. Type: multiselect
#. Description
#: ../templates:1001
msgid "Choose which blend to install:"
msgstr "Welcher Blend soll installiert werden?"

#. Type: multiselect
#. Description
#. Type: multiselect
#. Description
#: ../templates:1001 ../templates:2001
msgid "Debian can be widely adapted for people with specific needs."
msgstr ""
"Debian kann weithin angepasst werden für Anwender mit speziellen "
"Bedürfnissen."

#. Type: multiselect
#. Description
#. Type: multiselect
#. Description
#: ../templates:1001 ../templates:2001
msgid ""
"For some use cases there are the Debian Pure Blends, which provide ready-to-"
"install meta packages, to ease this task."
msgstr ""
"Für einige Anwendungsfälle existieren die Debian Pure Blends, "
"mit fertigen Metapaketen, um die Installation zu vereinfachen."

#. Type: multiselect
#. Description
#. Type: multiselect
#. Description
#: ../templates:1001 ../templates:2001
msgid "Here you can choose which of these blends to install."
msgstr ""
"Hier können Sie auswählen, welcher dieser Blends installiert werden soll."

#. Type: title
#. Description
#: ../templates:3001
msgid "Software blends selection"
msgstr "Blends-Softwareauswahl"
