# Translation of tasksel/debian to Finnish
# Translators, if you are not familiar with the PO format, gettext
# documentation is worth reading, especially sections dedicated to
# this format, e.g. by running:
# info -n '(gettext)PO Files'
# info -n '(gettext)Header Entry'
# Some information specific to po-debconf are available at
# /usr/share/doc/po-debconf/README-trans
# or http://www.debian.org/intl/l10n/po-debconf/README-trans
# Developers do not need to manually edit POT or PO files.
# Tapio Lehtonen 2004
# , fuzzy
#
#
msgid ""
msgstr ""
"Project-Id-Version: tasksel 2.00\n"
"Report-Msgid-Bugs-To: blendsel@packages.debian.org\n"
"POT-Creation-Date: 2025-01-09 09:02+0100\n"
"PO-Revision-Date: 2006-09-11 13:44+0300\n"
"Last-Translator: Tapio Lehtonen <tale@debian.org>\n"
"Language-Team: Finnish <debian-l10n-finnish@lists.debian.org>\n"
"Language: fi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: multiselect
#. Description
#: ../templates:1001
#, fuzzy
#| msgid "Choose software to install:"
msgid "Choose which blend to install:"
msgstr "Valitse asennettavat ohjelmat:"

#. Type: multiselect
#. Description
#. Type: multiselect
#. Description
#: ../templates:1001 ../templates:2001
msgid "Debian can be widely adapted for people with specific needs."
msgstr ""

#. Type: multiselect
#. Description
#. Type: multiselect
#. Description
#: ../templates:1001 ../templates:2001
msgid ""
"For some use cases there are the Debian Pure Blends, which provide ready-to-"
"install meta packages, to ease this task."
msgstr ""

#. Type: multiselect
#. Description
#. Type: multiselect
#. Description
#: ../templates:1001 ../templates:2001
#, fuzzy
#| msgid "Choose software to install:"
msgid "Here you can choose which of these blends to install."
msgstr "Valitse asennettavat ohjelmat:"

#. Type: title
#. Description
#: ../templates:3001
#, fuzzy
#| msgid "Software selection"
msgid "Software blends selection"
msgstr "Ohjelmavalikoima"

#, fuzzy
#~| msgid "Choose software to install:"
#~ msgid "Blends to install:"
#~ msgstr "Valitse asennettavat ohjelmat:"

#~ msgid ""
#~ "At the moment, only the core of the system is installed. To tune the "
#~ "system to your needs, you can choose to install one or more of the "
#~ "following predefined collections of software."
#~ msgstr ""
#~ "Tällä hetkellä vain Debianin peruskokoonpano on asennettuna. Voit muokata "
#~ "asennusta tarpeittesi mukaan valitsemalla asennettavaksi yhden tai "
#~ "useamman seuraavista ennalta määritellyistä ohjelmakokoelmista."

#~ msgid ""
#~ "You can choose to install one or more of the following predefined "
#~ "collections of software."
#~ msgstr ""
#~ "Voit valita asennettavaksi yhden tai useamman seuraavista ennalta "
#~ "määritellyistä ohjelmakokoelmista."

#~ msgid "${ORIGCHOICES}"
#~ msgstr "${CHOICES}"

#~ msgid "${CHOICES}, manual package selection"
#~ msgstr "${CHOICES}, valitse paketit itse"
