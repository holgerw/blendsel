#
msgid ""
msgstr ""
"Project-Id-Version: Debian-installer HR\n"
"Report-Msgid-Bugs-To: blendsel@packages.debian.org\n"
"POT-Creation-Date: 2025-01-09 09:02+0100\n"
"PO-Revision-Date: 2021-04-23 17:22+0200\n"
"Last-Translator: Valentin Vidic <vvidic@debian.org>\n"
"Language-Team: Croatian <lokalizacija@linux.hr>\n"
"Language: hr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: multiselect
#. Description
#: ../templates:1001
#, fuzzy
#| msgid "Choose software to install:"
msgid "Choose which blend to install:"
msgstr "Odaberite programe za instalaciju:"

#. Type: multiselect
#. Description
#. Type: multiselect
#. Description
#: ../templates:1001 ../templates:2001
msgid "Debian can be widely adapted for people with specific needs."
msgstr ""

#. Type: multiselect
#. Description
#. Type: multiselect
#. Description
#: ../templates:1001 ../templates:2001
msgid ""
"For some use cases there are the Debian Pure Blends, which provide ready-to-"
"install meta packages, to ease this task."
msgstr ""

#. Type: multiselect
#. Description
#. Type: multiselect
#. Description
#: ../templates:1001 ../templates:2001
#, fuzzy
#| msgid "Choose software to install:"
msgid "Here you can choose which of these blends to install."
msgstr "Odaberite programe za instalaciju:"

#. Type: title
#. Description
#: ../templates:3001
#, fuzzy
#| msgid "Software selection"
msgid "Software blends selection"
msgstr "Odabir programa"

#, fuzzy
#~| msgid "Choose software to install:"
#~ msgid "Blends to install:"
#~ msgstr "Odaberite programe za instalaciju:"

#~ msgid "This can be preseeded to override the default desktop."
#~ msgstr ""
#~ "Pretpostavljeno radno okruženje možete promijeniti putem preeseed "
#~ "postavke."

#~ msgid ""
#~ "At the moment, only the core of the system is installed. To tune the "
#~ "system to your needs, you can choose to install one or more of the "
#~ "following predefined collections of software."
#~ msgstr ""
#~ "Trenutno je instaliran samo osnovni sustav. Kako bi prilagodili "
#~ "instalaciju svojim potrebama, možete odabrati instalaciju sljedećih "
#~ "predodređenih skupina programa."

#~ msgid ""
#~ "You can choose to install one or more of the following predefined "
#~ "collections of software."
#~ msgstr ""
#~ "Možete odabrati jednu ili više sljedećih predodređenih skupina programa."
